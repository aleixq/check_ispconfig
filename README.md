Utility that takes configuration from ispconfig_mysql.yaml to obtain the list of active databases in ispconfigs, then
dumps to stdout the checks over these databases. 

It also takes configuration from ispconfig_mysql.yaml to obtain list of active web sites directories in ispconfig, then dumps to stdout the checks over these.

If borgmatic base configuration is specified then it can merge the configurations. 

Examples of ymls are provided.

```
usage: check_ispconfig [-h] [-i ISP]
                               [-s MYSQL_SERVERS] [-w WEB_SERVERS] [-x] [-d]
                               [-f] [-c]

optional arguments:
  -h, --help            show this help message and exit
  -i ISP, --isp ISP     ispconfig database configuration path (default:
                        /etc/check_ispconfig/ispconfig_mysql.yaml)
  -s MYSQL_SERVERS, --mysql_servers MYSQL_SERVERS
                        mysql servers yaml configuration path (default:
                        /etc/check_ispconfig/mysql_servers.yaml)
  -w WEB_SERVERS, --web_servers WEB_SERVERS
                        web servers yaml configuration path (default:
                        /etc/check_ispconfig/web_servers.yaml)
  -x, --lxc             if it's there the lxc rootfs will be prefixed in all
                        paths of the web_servers it makes sense to be used in
                        conjunction with -w. (default: False)
  -d, --dbs             if it's there it will try to build a list of mysql
                        databases (default: False)
  -f, --files           if a list of website directories must be built.
                        (default: False)
  -c, --check_http      checks the http status defined for each active website.
  -e EXCLUDE, --exclude EXCLUDE
                        exclude the sites whose the domain match with this regexp. (default: None)  
```

# Installation

install python3-mysql.connector and aiohttp

for tags:

`pip3 install https://gitlab.com/communia/check_ispconfig/-/archive/v0.1/check_ispconfig-v0.1.zip`

or master:

`pip3 install https://gitlab.com/communia/check_ispconfig/-/archive/master/check_ispconfig-master.zip`

