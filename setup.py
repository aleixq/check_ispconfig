from setuptools import setup, find_packages

setup(name='check_ispconfig',
      version='0.2',
      description='checks the ispconfig databases,websites or website directories (optionally with lxc rootfs)',
      url='http://gitlab.com/aleixq/check_ispconfig',
      author='Aleix Quintana Alsius',
      author_email='kinta@communia.org',
      license='GPL',
      packages=["check_ispconfig"],
      scripts=['bin/check_ispconfig'],
      zip_safe=False)
